require('cypress-iframe');

Cypress.Commands.add('inViewport', { prevSubject: true }, subject => {
  return cy.window().then((window) => {
    const { viewportHeight } = getViewPortDimensions(window);
    const boundingBox = subject[0].getBoundingClientRect();

    expect(boundingBox.top).to.be.at.least(-10);
    expect(boundingBox.left).to.be.at.least(0);
    expect(boundingBox.top).to.be.lessThan(viewportHeight);
    expect(boundingBox.bottom).to.be.lte(viewportHeight); //less or equal

    return subject;
  });
});

Cypress.Commands.add('notInViewport', { prevSubject: true }, (subject) => {
  return cy.window().then((window) => {
    const {viewportHeight} = getViewPortDimensions(window);
    const boundingBox = subject[0].getBoundingClientRect();

    expect(boundingBox.top).not.to.be.within(0, viewportHeight);

    return subject;
  });
});

Cypress.Commands.add('isBottomOfScreen', () => {
  return cy.window().then((window) => {
    return (window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 2
  });
});

/**
 *
 * @param {Window} win
 * @returns {{viewportWidth: number, viewportHeight: number}}
 */
const getViewPortDimensions = (win) => {
  const viewportWidth = win.innerWidth || win.document.documentElement.clientWidth;
  const viewportHeight = win.innerHeight || win.document.documentElement.clientHeight;

  return {
    viewportWidth,
    viewportHeight
  };
}

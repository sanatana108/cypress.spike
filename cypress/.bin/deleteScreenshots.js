const del = require('del');

const screenShotsFolder = 'cypress/screenshots/**';
const videoFolder = 'cypress/videos/**';

del.sync([screenShotsFolder, videoFolder]);


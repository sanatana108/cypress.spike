const glob = require('glob');
const path = require("path");

class DocumentsService {
  interpreterCollection = [];

  constructor() {
    let interpreter;
    let name;

    glob.sync('./cypress/integration/common/**/*.pattern.js').forEach((file) => {
      interpreter = require(path.resolve(file));
      name = this.convertPatternName(path.basename(file));
      this.interpreterCollection.push( {
        name,
        patterns: this.convertPatternsToObject(interpreter),
      });
    });
  }

  getPatterns = () => {
    return this.interpreterCollection;
  }

  convertPatternName = (text) => {
    const result = text.replace(/\.pattern\.js/, '').replace(/([A-Z])/g, " $1");
    return `${result.charAt(0).toUpperCase()}${result.slice(1)}`;
  }

  convertPatternsToObject = (interpreter) => {
    const patternCollection = [];

    for (const [key, value] of Object.entries(interpreter.patterns)) {
      patternCollection.push(
        {
          id: key,
          pattern: value.toString(),
          examples: interpreter.examples[key],
          usage: interpreter.usage[key],
        }
      );
    }

    return patternCollection;
  }
}

module.exports = DocumentsService;

const BaseController = require('../base.controller');

/**
 * @function
 * @type HomeController
 */
class HomeController extends BaseController {
  documentsService;

  /**
   *
   * @param {DocumentsService} documentsService
   */
  constructor(documentsService) {
    super();
    this.documentsService = documentsService;
  }

  doGet = (req, res) => {
    return res.json(this.documentsService.getPatterns());
  }
}

module.exports = HomeController;

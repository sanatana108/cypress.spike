const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const BaseApi = require('./system/base_api');
const services = require('./providers/service.provider');
const appRoutes = require('./providers/route.provider');

/**
 * @class
 * @type {API}
 * @extends {BaseApi} BaseApi
 */
class API extends BaseApi {
  constructor() {
    const app = express();
    super(app);
    this.services = services();
    this.setMiddleWare();
    this.loadRoutes(this.services);
  }

  setMiddleWare() {
    this.app.use(bodyParser.json({extended: true, strict: true}));
    this.app.use(bodyParser.urlencoded({extended: true}));
    this.app.use(bodyParser.text({extended: true}));
    this.app.use(cors());
  }

  /**
   *
   * @param {Jimple<ServiceProvider>} servicesProvider
   */
  loadRoutes(servicesProvider) {
    appRoutes(
      this.app,
      servicesProvider,
    );
  }
}

module.exports = API;

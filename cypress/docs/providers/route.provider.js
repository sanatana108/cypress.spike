const glob = require('glob');
const path = require('path');

module.exports = ((app, services) => {
  let route;

  glob.sync('./cypress/docs/routes/**/*.route.js').forEach((file) => {
    route = require(path.resolve(file));
    route(app, services);
  });

  app.all('/favicon.ico', (req, res) => {
    res.status(204).send('');
  });
});

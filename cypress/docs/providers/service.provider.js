const Jimple = require('@00f100/jimple');

const DocumentsService = require('../services/documents.service');

/**
 * @function {ServiceProvider}
 * @type {function(*=, *): Jimple}
 */
module.exports = () => {
  const diContainer = new Jimple();

  diContainer.set('documents', () => new DocumentsService());
  return diContainer;
};

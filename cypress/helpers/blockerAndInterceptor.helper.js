const js = require('../helpers/rewritter/js');

const getUrlForIntercept = (inputUrl) => {
  if (isUrl(inputUrl)) {
    return inputUrl;
  }

  return new RegExp(inputUrl, 'i');
}

const interceptRewriteUrls = () => {
  const rewriteUrls = [ ...Cypress.env('forceRewriteJs') || [], ...Cypress.config('forceRewriteJs') || []];
  let url;

  if (Array.isArray(rewriteUrls)) {
    rewriteUrls.forEach((rewriteUrl) => {
      if (typeof rewriteUrl !== 'string') {
        return;
      }

        url = getUrlForIntercept(rewriteUrl);

        cy.intercept({
          method: 'GET',
          url
        }, (req) => {
          req.continue((res) => {
            res.body = (js.rewriteJs(req.url, res.body));
          });
        }).as(`override ${url.toString()}`)
      });
  }
}

const interceptBlockedUrls = () => {
  const blockUrls = [ ...Cypress.env("blockedUrls") || [], ...Cypress.config('blockedUrls') || []];
  const urls = [];
  let method;
  let urlMethodId;
  let url;

  if (Array.isArray(blockUrls)) {
    blockUrls.forEach((blockedUrl) => {
      method = getValidMethod(blockedUrl.method);
      url = getUrlForIntercept(blockedUrl.url);
      urlMethodId = `${method}${url.toString()}`;

      if (!urls.includes(urlMethodId)) {
        cy.intercept(method, url,
        {
          statusCode: blockedUrl.statusCode || 204,
          body: blockedUrl.responseBody || null,
        }).as(`BLOCK: ${urlMethodId}`);

        urls.push(urlMethodId);
      }
    });
  }
}

const isValidMethod = (method) => {
  const validMethods = ['GET', 'POST', 'DELETE', 'PATCH', 'PUT'];
  return validMethods.includes(method);
}

const getValidMethod = (method) => {
  return isValidMethod(method || null) ? method : 'GET';
}

const blockUrls = () => {
  interceptBlockedUrls();
  interceptRewriteUrls();
}

const isUrl = (url) => {
  const isUrl = new RegExp('^https?:');
  return isUrl.test(url);
}

const getDestinationUrl = (targetUrl) => {

  if (isUrl(targetUrl)) {
    return targetUrl;
  }

  const baseUrl = Cypress.env('baseUrl');

  if (targetUrl === 'homepage') {
    return `${baseUrl}`;
  }

  return `${baseUrl}${targetUrl}`;
}

module.exports = {
  blockUrls,
  getDestinationUrl,
}

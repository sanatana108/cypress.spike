const getCorrectCondition = (condition) => {

  if (typeof condition !== 'string') {
    condition = '';
  }

  switch (condition.toString().toLowerCase()) {
    case 'at least':
      return 'to.have.length.of.at.least'
      break;

    case 'at most':
    case 'not more':
      return 'to.have.length.of.at.most';
      break;

    case 'only':
    case 'exactly':
    default:
      return 'to.have.lengthOf';
      break;
  }
};

module.exports = {
  getCorrectCondition,
};

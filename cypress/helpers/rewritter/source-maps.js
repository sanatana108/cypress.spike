const sourceMapRe = /\n\/\/[ \t]*(?:#|@) sourceMappingURL=([^\s]+)\s*$/
const dataUrlRe = /^data:application\/json;(?:charset=utf-8;)base64,([^\s]+)\s*$/

/**
 *
 * @param {string} js
 */
const getMappingUrl = (js) => {
  const matches = sourceMapRe.exec(js)

  if (matches) {
    return matches[1]
  }

  return undefined
}

/**
 *
 * @param {string} js
 * @returns {string}
 */
const stripMappingUrl = (js) => {
  return js.replace(sourceMapRe, '')
}

/**
 *
 * @param {string} url
 * @returns {any}
 */
const tryDecodeInlineUrl = (url) => {
  const matches = dataUrlRe.exec(url)

  if (matches) {
    try {
      const base64 = matches[1]
      return JSON.parse(Buffer.from(base64, 'base64').toString())
    } catch {
      return
    }
  }
}

/**
 *
 * @param {string} urlStr
 * @returns {{sourceRoot: undefined, sourceFileName: string, sourceMapName: string}|{sourceRoot: *, sourceFileName: string, sourceMapName: string}}
 */
const getPaths = (urlStr) => {
  try {
    const parsed = url.parse(urlStr, false)

    // if the sourceFileName is the same as the real filename, Chromium appends a weird "? [sm]" suffix to the filename
    // avoid this by appending some text to the filename
    // @see https://cs.chromium.org/chromium/src/third_party/devtools-frontend/src/front_end/sdk/SourceMap.js?l=445-447&rcl=a0c450d5b58f71b67134306b2e1c29a75326d3db
    const sourceFileName = `${path.basename(parsed.path || '')} (original)`
    parsed.pathname = path.dirname(parsed.pathname || '')
    delete parsed.search
    return { sourceRoot: parsed.format(), sourceFileName, sourceMapName: `${sourceFileName}.map` }
  } catch {
    return { sourceRoot: undefined, sourceFileName: 'source.js', sourceMapName: 'source.js.map' }
  }
}

/**
 *
 * @param {string} url
 * @param {string} js
 * @returns {string}
 */
const urlFormatter = (url, js) => {
  return [
    stripMappingUrl(js),
    `//# sourceMappingURL=${url}`,
  ].join('\n')
}

module.exports = {
  getMappingUrl,
  stripMappingUrl,
  tryDecodeInlineUrl,
  getPaths,
  urlFormatter
}

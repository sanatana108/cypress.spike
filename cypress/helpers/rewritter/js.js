import * as astTypes from 'ast-types'
import Debug from 'debug'
import { jsRules } from './js-rules'
import * as recast from 'recast'
import * as sourceMaps from './source-maps'

const debug = Debug('cypress:rewriter:js');

const defaultPrintOpts = {
  quote: 'single',
}

function _generateDriverError (url, err) {
  const args = JSON.stringify({
    errMessage: err.message,
    errStack: err.stack,
    url,
  })

  return `window.top.Cypress.utils.throwErrByPath('proxy.js_rewriting_failed', { args: ${args} })`
}

/**
 *
 * @param {string} url
 * @param {string} js
 * @param {any} inputSourceMap
 * @returns {{err}|any}
 */
const rewriteJsSourceMap = (url, js, inputSourceMap) => {
  try {
    const { sourceFileName, sourceMapName, sourceRoot } = sourceMaps.getPaths(url)

    const ast = recast.parse(js, { sourceFileName })

    astTypes.visit(ast, jsRules)

    return recast.print(ast, {
      inputSourceMap,
      sourceMapName,
      sourceRoot,
      ...defaultPrintOpts,
    }).map;
  } catch (err) {
    debug('error while parsing JS %o', { err, js: js.slice ? js.slice(0, 500) : js })
    return { err }
  }
}

/**
 *
 * @param {string} url
 * @param {string} js
 * @param {string} currentPageUrl
 * @private
 */
const _rewriteJsUnsafe = (url, js, currentPageUrl) => {

  if ((/# sourceMappingURL|source rewritten/).test(js)) {
    cy.log('JS source already captured...')
    return js;
  }

  const ast = recast.parse(js)

  try {
    astTypes.visit(ast, jsRules)
  } catch (err) {
    return _generateDriverError(url, err)
  }

  const { code } = recast.print(ast, defaultPrintOpts)
  return `${sourceMaps.stripMappingUrl(code)}\n//source rewritten by custom cypress JS rewriter\n` +
    `// original file: ${JSON.stringify(new URL(url))} \n`;
}

/**
 *
 * @param {string} url
 * @param {string} js
 * @returns {string|*|{slice}}
 */
const rewriteJs = (url, js, currentPageUrl) => {
  try {
    // rewriting can throw on invalid JS or if there are bugs in the js-rules, so always wrap it
    return _rewriteJsUnsafe(url, js, currentPageUrl);
  } catch (err) {
    debug('error while parsing JS %o', { err, js: js.slice ? js.slice(0, 500) : js })
    return js
  }
}

module.exports = {
  rewriteJsSourceMap,
  rewriteJs,
}

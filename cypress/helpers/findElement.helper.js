const elementFindTimeoutSeconds = Cypress.env('elementFindTimeoutSeconds')
  || Cypress.config('elementFindTimeoutSeconds')
  || 1;
const elementFindAttempts = Cypress.env('elementFindAttempts')
  || Cypress.config('elementFindAttempts')
  || 60;

const findElement = (selector, countAttempts) => {
  checkConfigValues();
  return cy.get(selector)
    .then((el) => {
      if (el.length < 1) {
        cy.wait(elementFindTimeoutSeconds * 1000);
        if (countAttempts >= elementFindAttempts) {
          expect(el.length).to.be.greaterThan(1, `Field ${selector} could not be located`);
        }
        return findElement(selector, countAttempts + 1)
      }
    });
}

const findVisibleElement = (selector, countAttempts) => {
  checkConfigValues();
  if (!selector.includes(':visible')) {
    selector = `${selector}:visible`;
  }
  return findElement(selector, 1);
}

const checkConfigValues = () => {
  expect(elementFindTimeoutSeconds).to.be.greaterThan(
      0,
    'Cypress config elementFindTimeoutSeconds'
  );

  expect(elementFindAttempts).to.be.greaterThan(
    0,
    'Cypress config elementFindAttempts'
  );
}

module.exports = {
  findElement,
  findVisibleElement,
}

const isMatchCase = (contentOption) => {
  let matchCase = ['keysensitive', 'sensitive', '', null, false].includes(contentOption);
  return matchCase;
};

module.exports = {
  isMatchCase,
}

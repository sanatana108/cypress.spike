Feature: Google How it works page

  I want to open a search engine

  Background:
    Given Viewport dimensions 1000x1500
    Given I open a "https://www.google.com/search/howsearchworks/?fg=1" in the browser

  Scenario: Open a search engine page
    Then I see "Google" in the title
    And I see button with text "I agree"
    Then I take a screenshot

  Scenario: Confirm Privacy policy
    When I click on I agree button (#L2AGLb)
    Then I agree button (#L2AGLb) is not visible

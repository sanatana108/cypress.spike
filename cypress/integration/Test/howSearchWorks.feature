Feature: Google How Search works page

  As a google user I want to open a how search works page,
  so I can learn how to use search functionality efficiently

  Background:
    Given Viewport dimensions 1000x1500
    Given I open page "https://www.google.com/search/howsearchworks/?fg=1" in the browser

  Scenario: Open a how it works page
    Then I see "Google Search – Discover how Google Search works" in the title
    When I scroll to the top of the page
    Then Footer element (footer.footer) is not in the viewport

    When I scroll to the bottom of the page
    Then Footer element (footer.footer) is in the viewport

    Then I scroll to the top of the page
    Then I see "Google Search – Discover how Google Search works" in the title
    Then Footer element (footer.footer) is not in the viewport

    When I scroll to the bottom of the page
    Then Footer (footer.footer) is in the viewport

    When I scroll to the top of the page
    And I wait 0.5 second
    Then Footer (footer.footer) is not in the viewport

    When I scroll to a bottom of page
    And I wait 1 second
    Then Footer (footer.footer) is visible in the viewport

    When I scroll to top of a page
    And I wait 1 second
    Then Footer (footer.footer) is not visible in the viewport

    When I scroll to bottom of the website
    And I wait 0.5 seconds
    Then Footer (footer.footer) is visible in viewport

    When i scroll to top of the page
    And i wait 1 second
    Then page footer (footer.footer) is not visible in a viewport

    Then Link "it starts with..." (.homepage-hero__content__buttons) is visible in the viewport
    And Video animation search prompt (.homepage-hero__content__images) is visible
    And Video animation search prompt (.homepage-hero__content__images) is in the viewport
    And Hero section (#hero) is in the viewport

    When I scroll to story of search section (#story-of-search)
    Then I see text "Watch our film: Trillions of Questions, No Easy Answers"
    And Play button (.centered-video__tout__youtube-icon) is visible in the viewport

    When I click on play button (.centered-video__tout__youtube-icon)
    And I wait 5 seconds
    Then Video (video.html5-main-video) is playing in iframe (.centered-video__media__iframe)
    And I wait 5 seconds
    When I stop video by clicking on it (video.html5-main-video) playing in iframe (.centered-video__media__iframe)
    Then Video (video.html5-main-video) is NOT playing in iframe (.centered-video__media__iframe)

    When I scroll to the top of the page
    Then I see text "This site uses cookies from Google to deliver its services and to analyze traffic."
    And I wait 2 seconds

    When I scroll to the bottom of the page
    Then I see text "This site uses cookies from Google to deliver its services and to analyze traffic."
    And I wait 2 seconds
    Then I see text "This site uses cookies from Google to deliver its services and to analyze traffic." in the viewport

    When I click on button OK (.cookieBarButton.cookieBarConsentButton) to accept cookie policy
    Then Text "This site uses cookies from Google to deliver its services and to analyze traffic." is not present
    Then Text "This site uses cookies from Google to deliver its services and to analyze traffic." is not present on the page
    Then Text "This site uses cookies from Google to deliver its services and to analyze traffic." is not present in current page

    When I scroll to the top of the page
    Then Text "This site uses cookies from Google to deliver its services and to analyze traffic." is not present

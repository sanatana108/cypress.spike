Feature: Encore tickets happy patch

  As a encore tickets user I want to open search for a product,
  add it to the basket, select seats, and proceed to the checkout page
  so I can purchase tickets

  Background:
    Given Viewport dimensions 640x400

  Scenario: Full user journey
    Given I visit page "https://littleblueboy.net/cypress/" in browser
    When I click on demo link (a[href="demo1.html"])

    When I type "test" into search field (input[type=text])
    When I click on submit button (input[type=submit])

    Then I see text "You have reached"
    When I click on JS Event Link (#jslink)

    Then I see text "You have followed JS link" to be visible in the viewport

    #Then I intercept request
    Then I click on Break Cypress link ([data-test_id="break_cypress"])
    Then I see text "Followed break cypress link"

    Then URL should include "cypress/demo4.html"
    #Then I click on Break Cypress link ([data-test_id="break_cypress"])
    Then I click on Break Cypress link ([data-test_id="break_cypress_2"])
    Then I see text "Followed break cypress link"
    Then URL should include "cypress/demo5.html"
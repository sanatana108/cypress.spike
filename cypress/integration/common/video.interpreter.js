import { Given, And, Then } from 'cypress-cucumber-preprocessor/steps';

const {
  videoIsPlayingPattern,
  videoInIframeIsPlayingPattern,
  videoInIframeIsNotPlayingPattern,
  stopVideoInIframePattern
} = require('./patterns/video.pattern').patterns;


When(stopVideoInIframePattern, (optionalText, videoSelector, playing, frame, iframeSelector) => {
  stopVideoIsPlayingInIframe(videoSelector, iframeSelector)
});

And(stopVideoInIframePattern, (optionalText, videoSelector, playing, frame, iframeSelector) => {
  stopVideoIsPlayingInIframe(videoSelector, iframeSelector)
});

Then(videoInIframeIsPlayingPattern, (optional, videoSelector, optionalFrameText, iframeSelector) => {
  testVideoIsPlayingInIframe(videoSelector, iframeSelector)
});

And(videoInIframeIsPlayingPattern, (optional, videoSelector, optionalFrameText, iframeSelector) => {
  testVideoIsPlayingInIframe(videoSelector, iframeSelector)
});

Then(videoInIframeIsNotPlayingPattern, (optional, videoSelector, optionalFrameText, iframeSelector) => {
  testVideoIsNotPlayingInIframe(videoSelector, iframeSelector)
});

And(videoInIframeIsNotPlayingPattern, (optional, videoSelector, optionalFrameText, iframeSelector) => {
  testVideoIsNotPlayingInIframe(videoSelector, iframeSelector)
});

Then(videoIsPlayingPattern, (optional, selector) => {
  testVideoIsPlaying(selector);
});

And(videoIsPlayingPattern, (optional, selector) => {
  testVideoIsPlaying(selector);
});

Given(videoIsPlayingPattern, (optional, selector) => {
  testVideoIsPlaying(selector);
});

const testVideoIsPlaying = (selector) => {
  cy.get(selector)
    .should('have.prop', 'paused', false)
    .and('have.prop', 'ended', false);
};

const testVideoIsPlayingInIframe = (videoSelector, iframeSelector) => {
  cy.frameLoaded(`${iframeSelector}`)
    .its('0.contentDocument')
    .should('exist')
    .iframe()
    .find(`${videoSelector}`)
    .should('have.prop', 'paused', false)
    .and('have.prop', 'ended', false)
};

const testVideoIsNotPlayingInIframe = (videoSelector, iframeSelector) => {
  cy.frameLoaded(`${iframeSelector}`)
    .its('0.contentDocument')
    .should('exist')
    .iframe()
    .find(`${videoSelector}`)
    .should('have.prop', 'paused', true)
};

const stopVideoIsPlayingInIframe = (videoSelector, iframeSelector) => {
  cy.frameLoaded(`${iframeSelector}`)
    .its('0.contentDocument')
    .should('exist')
    .iframe()
    .find(`${videoSelector}`)
    .should('have.prop', 'paused', false)
    .and('have.prop', 'ended', false)
    .click();
};

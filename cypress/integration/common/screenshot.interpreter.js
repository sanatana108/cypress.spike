import { And, Then } from 'cypress-cucumber-preprocessor/steps';

const { screenShootPattern } = require('./patterns/screenshort.pattern').patterns;

And(screenShootPattern, () => {
  takeScreenShot();
});

Then(screenShootPattern, () => {
  takeScreenShot();
});

const takeScreenShot = () => {
  cy.screenshot();
};

const { Given, Then } = require('cypress-cucumber-preprocessor/steps');
const {
  openUrlPattern,
  openHomePagePattern,
  urlShouldBePattern,
  urlShouldIncludePattern
} = require('./patterns/browserNavigation.pattern').patterns;

import { getDestinationUrl, blockUrls } from '../../helpers/blockerAndInterceptor.helper';

Given(openUrlPattern, (action, optional1, optional2, url) => {
  blockUrls();
  visitUrl(url);
});

Given(openHomePagePattern, (action, optional, url) => {
  blockUrls();
  visitUrl(url);
});

Then(urlShouldBePattern, function(value) {
  testUrlShouldBe(value);
});

And(urlShouldBePattern, (value) => {
  testUrlShouldBe(value);
});

Then(urlShouldIncludePattern, (value) => {
  testUrlShouldInclude(value);
});

And(urlShouldIncludePattern, (value) => {
  testUrlShouldInclude(value);
});

const testUrlShouldBe = (value) => {
  cy.url()
    .should('eq', value);
}

const testUrlShouldInclude = (value) => {
  cy.url()
    .should('contain', value);
}

const visitUrl = (url) => {
  cy.visit(getDestinationUrl(url));
}

import { And, When } from 'cypress-cucumber-preprocessor/steps';

const {
  scrollTopOrBottomPattern,
  scrollToPattern
} = require ('../common/patterns/scroll.pattern').patterns;

And(scrollTopOrBottomPattern, (optionalText, direction) => {
  scrollToExtreme(direction);
});

When(scrollTopOrBottomPattern, (optionalText, direction) => {
  scrollToTop(direction);
});

When(scrollToPattern, (element, selector, options) => {
  scrollToElement(selector, options);
});

And(scrollToPattern, (element, selector, options) => {
  scrollToElement(selector, options);
});

const scrollToExtreme = (direction) => {
  if (direction.toLowerCase() === 'bottom') {
    scrollToBottom();
    return;
  }

  if (direction.toLowerCase() === 'top') {
    scrollToTop();
    return;
  }
}

const scrollToElement = (selector, options) => {
  return cy.get(`${selector}`)
    .should('be.visible')
    .scrollIntoView()
    .then((el) => {
      expect(el.length).to.be.eq(1);
      return isInViewport(el);
    }).then((inViewport) => {
      if (!inViewport) {
        if (options === 'nocheck') {
          return;
        }

        cy.log('element not in viewport');

        return cy.wait(1000)
          .then(() => {
            return scrollToElement(selector);
          });
      }
    });
};

const scrollToBottom = () => {
  return cy.scrollTo('bottom', { duration: 1 })
    .wait(1000)
    .then(() => {
      return cy.window().then((window) => {
        return cy.scrollTo('bottom').wait(500).isBottomOfScreen()
          .then((isAtTheBottom) => {
            if (!isAtTheBottom) {
              return scrollToBottom();
            }
        });
      });
  });
}

const scrollToTop = () => {
  return cy.scrollTo('top', { duration: 1 })
    .wait(1000 );
}

const isInViewport = (el) => {
  return cy.window()
    .then((win) => {
    const viewportHeight = win.innerHeight || win.document.documentElement.clientHeight;
    const boundingBox = el[0].getBoundingClientRect();

    if (
      boundingBox.top >= -10 &&
      boundingBox.left >= 0 &&
      boundingBox.bottom <= viewportHeight
    ) {
      return true;
    } else {
      return false;
    }
  });
}

import { When, And, Then } from "cypress-cucumber-preprocessor/steps";
const { inputPattern, inputValuePattern, enterPattern } = require('./patterns/input.pattern').patterns;
const { findElement } = require('../../helpers/findElement.helper');
const { isMatchCase } = require('../../helpers/textOptions.helper');

When(inputPattern, (action, value, optional, selector) => {
  typeIntoField(selector, value);
});

And(inputPattern, (action, value, optional, selector) => {
  typeIntoField(selector, value);
});

Then(inputValuePattern, (optionalArticle, optionalText, selector, expectedValue, expectedValueOptions) => {
  testInputValue(selector, expectedValue, expectedValueOptions)
});

And(inputValuePattern, (optionalArticle, optionalText, selector, expectedValue, expectedValueOptions) => {
  testInputValue(selector, expectedValue, expectedValueOptions)
});

Then(enterPattern, (optionalText, optionalText2, selector) => {
  pressEnter(selector);
});

And(enterPattern, (optionalText, optionalText2, selector) => {
  pressEnter(selector);
});

const pressEnter = (selector) => {
  cy.get(`${selector}`)
    .should('be.visible')
    .type('{enter}');
};

Given(/Find (.*) \((.*)\)/i, (optionalText, selector) => {
  cy.get(`${selector}`);
});

const testInputValue = (selector, expectedValue, expectedValueOptions) => {
  const escapedString = escapeRegExp(expectedValue);
  const matchCase = isMatchCase(expectedValueOptions);
  const matchCaseFlag = matchCase ? '': 'i';
  const valueMatchRegex = new RegExp(escapedString, matchCaseFlag);

  cy.get(`${selector}`)
    .invoke('val')
    .should('match', valueMatchRegex);
}

const escapeRegExp = (string) => {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

const typeIntoField = (selector, value) => {
  const inputElement = findElement(selector);
  inputElement.type(value);
}

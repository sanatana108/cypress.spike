import { Then, And } from 'cypress-cucumber-preprocessor/steps';

const { findElement, findVisibleElement } = require('../../helpers/findElement.helper');
const { isMatchCase } = require('../../helpers/textOptions.helper');

const {
  elementWithTextPattern,
  contentIsInElementPattern,
  contentIsNotPresentPattern,
  elementIsNotVisiblePattern,
  elementIsVisiblePattern,
  elementWithContentIsVisiblePattern,
  elementWithContentIsNotVisiblePattern,
  waitForElementToBeVisiblePattern,
  waitForElementToBeAvailablePattern,
  textIsPresentPattern,
} = require('./patterns/visibility.pattern').patterns;

Then(textIsPresentPattern, (optionalText, content, matchCase) => {
  testElementWithTextVisibility(content, matchCase, null);
});

And(textIsPresentPattern, (optionalText, content, matchCase) => {
  testElementWithTextVisibility(content, matchCase, null);
});

Then(elementWithTextPattern, (optionalText, optionalSelector, optionalWith, optionalTextContent, content, matchCase) => {
  testElementWithTextVisibility(content, matchCase, optionalSelector);
});

And(elementWithTextPattern, (optionalText, optionalSelector, optionalWith, optionalTextContent, content, matchCase) => {
  testElementWithTextVisibility(content, matchCase, optionalSelector);
});

Then(contentIsInElementPattern, (option, content, contentOptions, optionalText, parentSelector) => {
  testContentIsInElement(parentSelector, content, contentOptions);
});

And(contentIsInElementPattern, (option, content, contentOptions, optionalText, parentSelector) => {
  testContentIsInElement(parentSelector, content, contentOptions);
});

Then (contentIsNotPresentPattern, (optionalText, content, contentOptions, optionalText2) => {
  testContentIsNotPresent(contentOptions, contentOptions);
});

And(contentIsNotPresentPattern, (optionalText, content, contentOptions, optionalText2) => {
  testContentIsNotPresent(contentOptions, contentOptions);
});

Then(waitForElementToBeVisiblePattern, (description, selector) => {
  waitForElementToBeVisible(selector);
});

And(waitForElementToBeVisiblePattern, (description, selector) => {
  waitForElementToBeVisible(selector);
});

Then(elementWithContentIsVisiblePattern, (what, selector, optionalText, optionalText2, content, matchCase) => {
  testElementWithContentIsVisible(selector, content, matchCase);
});

And(elementWithContentIsVisiblePattern, (what, selector, optionalText, optionalText2, content, matchCase) => {
  testElementWithContentIsVisible(selector, content, matchCase);
});

Then(elementWithContentIsNotVisiblePattern, (what, selector, optionalText, optionalText2, content, matchCase) => {
  testElementWithContentNotVisible(selector, content, matchCase);
});

And(elementWithContentIsNotVisiblePattern, (what, selector,  optionalText, optionalText2, content, matchCase) => {
  testElementWithContentNotVisible(selector, content, matchCase);
});

Then(elementIsVisiblePattern, (optionalText, selector) => {
  testElementIsVisible(selector);
});

And(elementIsVisiblePattern, (optionalText, selector) => {
  testElementIsVisible(selector);
});

Then(elementIsNotVisiblePattern, (what, selector) => {
  testElementIsNotVisible(selector);
});

And(elementIsNotVisiblePattern, (what, selector) => {
  testElementIsNotVisible(selector);
});

Then(waitForElementToBeAvailablePattern, (optionalText, selector, optionalText2, action) => {
  waitForElementToBeAvailable(selector);
});

And(waitForElementToBeAvailablePattern, (optionalText, selector, optionalText2, action) => {
  waitForElementToBeAvailable(selector);
});

const testElementWithTextVisibility = (content, matchCase, optionalSelector) => {
  let el = '';
  el = cy;

  if (optionalSelector) {
    el = cy.get(`${optionalSelector}`);
  }

  el.contains(content, { matchCase: isMatchCase(matchCase) })
    .should('have.length.of.at.least', 1)
    .should('be.visible');
}

const waitForElementToBeVisible = (selector) => {
  return findVisibleElement(selector, 1);
}

const waitForElementToBeAvailable = (selector) => {
  return findElement(selector, 1);
}

const testContentIsInElement = (selector, content, matchCase) => {
  return cy.get(`${selector}`)
    .contains(content,  { matchCase: isMatchCase(matchCase) })
    .should('have.length.at.least', 1);
};

const testElementIsNotVisible = (selector) => {
  cy.get(`${selector}`)
    .should('not.be.visible');
}

const testElementWithContentIsVisible = (selector, content, matchCase) => {
  cy.get(`${selector}`)
    .contains(content, { matchCase: isMatchCase(matchCase) })
    .should('be.visible');
}

const testElementWithContentNotVisible = (selector, content, matchCase) => {
  cy.get(`${selector}`)
    .contains(content, { matchCase: isMatchCase(matchCase) })
    .should('not.be.visible');
}

const testContentIsNotPresent = (content, contentOptions) => {
  cy.contains(content, { matchCase: isMatchCase(contentOptions) })
    .should('have.length.below', 1);
}

const testElementIsVisible = (selector) => {
  cy.get(`${selector}`)
    .should('be.visible');
}

const screenShootPattern = /I take ((a|the) )?screenshot/i;

module.exports = Object.freeze(
  {
    patterns: {
      screenShootPattern,
    },
    minimumExample: {
      screenShootPattern: 'I take screenshot',
    },
    examples: {
      screenShootPattern: [
        'I take a screenshot',
        'I take the screenshot',
        'I take screenshot',
      ],
    },
    usage: {
      screenShootPattern: ['Then', 'And'],
    }
  }
);

const videoIsPlayingPattern = /Video( (.*))?\((.*)\) is playing$/i;
const videoInIframeIsPlayingPattern = /Video( (.*))?\((.*)\) is playing in (frame|iframe) \((.*)\)/i;
const videoInIframeIsNotPlayingPattern = /Video( (.*))?\((.*)\) is not playing in (frame|iframe) \((.*)\)/i;
const stopVideoInIframePattern  = /I stop video( (.*))?\((.*)\)( (playing|.*))? in (frame|iframe) \((.*)\)/i;

module.exports = Object.freeze(
  {
    patterns: {
      videoIsPlayingPattern,
      videoInIframeIsPlayingPattern,
      videoInIframeIsNotPlayingPattern,
      stopVideoInIframePattern
    },
    minimumExample: {
      videoIsPlayingPattern: 'Video (CSS_SELECTOR) is playing',
      videoInIframeIsPlayingPattern: 'Video (CSS_SELECTOR) is playing in iframe (CSS_SELECTOR)',
      videoInIframeIsNotPlayingPattern: 'Video (CSS_SELECTOR) is not playing in iframe (CSS_SELECTOR)',
      stopVideoInIframePattern: 'I stop video (CSS_SELECTOR) in iframe (CSS_SELECTOR)',

    },
    examples: {
      videoIsPlayingPattern: [
        'Video (CSS_SELECTOR) is playing',
        'Video some description as optional text (CSS_SELECTOR) is playing'
        ],
      videoInIframeIsPlayingPattern: [
        'Video some description as optional text (CSS_SELECTOR) is playing in iframe (CSS_SELECTOR)'
      ],
      videoInIframeIsNotPlayingPattern: [
        'Video (CSS_SELECTOR) is not playing in iframe (CSS_SELECTOR)',
        'Video additional description (CSS_SELECTOR) is not playing in iframe (CSS_SELECTOR)',
      ],
      stopVideoInIframePattern: [
        'I stop video (CSS_SELECTOR) in iframe (CSS_SELECTOR)',
        'I stop video optional description (CSS_SELECTOR) in iframe (CSS_SELECTOR)',
      ]
    },
    usage: {
      videoIsPlayingPattern: ['Then', 'And'],
      videoInIframeIsPlayingPattern: ['Then', 'And'],
      videoInIframeIsNotPlayingPattern: ['Then', 'And'],
      stopVideoInIframePattern: ['Then', 'And'],
    }
  }
);

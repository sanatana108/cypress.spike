const clickPattern = /I click( (.*))? \((.*)\)( (.*))?/i;

module.exports = Object.freeze(
  {
    patterns: {
      clickPattern,
    },
    minimumExample: {
      clickPattern: 'I click (CSS_SELECTOR)',
    },
    examples: {
      clickPattern: [
        'I click (CSS_SELECTOR)',
        'I click (CSS_SELECTOR) to do something with the webpage',
        'I click on fist item in search results (CSS_SELECTOR) to open a new page'
      ],
    },
    usage: {
      clickPattern: ['When', 'And'],
    }
  }
);

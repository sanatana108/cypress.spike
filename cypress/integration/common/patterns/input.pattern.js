const aaa = '(\|(keyinsensitive))?';

const inputPattern = /I (type|enter) "(.*)" into( (.*))? \((.*)\)/i;
const inputValuePattern = /((The|a) )?Value of( (.*))? \((.*)\) is "(.*)"(\|.*)?/i;
const enterPattern = /I press enter( (.*))? on( (.*))? \((.*)\)/i;

module.exports = Object.freeze(
  {
    patterns: {
      inputPattern,
      inputValuePattern,
      enterPattern
    },
    minimumExample: {
      inputPattern: 'I type "value" into (CSS_SELECTOR)',
      inputValuePattern: 'Value of (CSS_SELECTOR) is "expected value"',
      enterPattern: 'I press enter on (CSS_SELECTOR)'
    },
    examples: {
      inputPattern: [
        'I type "value" into search field (CSS_SELECTOR)',
        'I enter "value" into (CSS_SELECTOR)',
      ],

      inputValuePattern: [
        'The value of (CSS_SELECTOR) is "Expected value of input field"',
        'The value of search input field (CSS_SELECTOR) is "Expected value of input field"',
      ],

      enterPattern: [
        'I press enter key on search in (CSS_SELECTOR)',
      ]
    },
    usage: {
      inputPattern: ['When', 'And'],
      inputValuePattern: ['Then', 'And'],
      enterPattern: ['And', 'Then']
    }
  }
);

const openUrlPattern = /I (open|visit)( (a|the))?( (page|url))? "(.*)" in( (a|the))? browser/i;
const openHomePagePattern = /I (open|visit)( (a|the))? (homepage) in( (a|the))? browser/i;
const urlShouldIncludePattern = /Url should include "(.*)"/i;
const urlShouldBePattern = /Url should be "(.*)"/i;

module.exports = Object.freeze(
  {
    patterns: {
      openUrlPattern,
      openHomePagePattern,
      urlShouldIncludePattern,
      urlShouldBePattern
    },
    minimumExample: {
      openUrlPattern: 'I open "https://www.google.com" in browser',
      openHomePagePattern: 'I open homepage in browser',
      urlShouldBePattern: 'Url should be "value"',
      urlShouldIncludePattern: 'Url should include "value"',
    },
    examples: {
      openUrlPattern: [
        'I open "https://www.google.com" in browser',
        'I open "/foo" in browser',
        'I visit "/foo" in a browser',
        'I visit "/foo" in the browser',
        'I open page "/foo" in the browser',
        'I open a page "/foo" in the browser',
        'I open the page "/foo" in the browser',
        'I open a url "/foo" in the browser',
      ],
      openHomePagePattern: [
        'I open homepage in browser',
        'I open homepage in a browser',
        'I open homepage in the browser',
        'I visit homepage in browser',
        'I visit homepage in a browser',
        'I visit homepage in the browser',
      ],
      urlShouldBePattern: [
        'Url should be "value"',
      ],
      urlShouldIncludePattern: [
        'Url should include "value"',
      ],
    },
    usage: {
      openUrlPattern: ['Given'],
      openHomePagePattern: ['Given'],
      urlShouldIncludePattern: ['Then', 'And'],
      urlShouldBePattern: ['Then', 'And']
    }
  }
);

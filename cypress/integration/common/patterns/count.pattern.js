const countPattern = /I expect(.*(at least|at most|only|not more))?( (.*))? (\d+)( (.*))?\((.*)\)/i;

module.exports = Object.freeze(
  {
    patterns: {
      countPattern,
    },
    minimumExample: {
      countPattern: 'I expect 1 (.cssIdentifier))',
    },
    examples: {
      countPattern: [
        'I expect 1 (CSS_SELECTOR)',
        'I expect to see 1 (CSS_SELECTOR)',
        'I expect to find at least 1 result (CSS_SELECTOR)',
        'I expect to see at most 1 result (CSS_SELECTOR)',
        'I expect to see only 2 results (CSS_SELECTOR)',
        'I expect to see not more than 2 results (CSS_SELECTOR)',
      ],
    },
    usage: {
      countPattern: ['When', 'And'],
    }
  }
);


const viewportPattern = /Viewport( (.*))? (\d+)x(\d+)/i;
const devicePattern = /Viewport (is|for|is for)( (device))? ([A-z0-9_\-\+]+)$/
const elementNotInViewportPattern = /(.*)?\((.*)\) is not( (visible))? in( (the|a))? viewport/i
const elementInViewportPattern = /(.*)?\((.*)\) is( (visible))? in( (the|a))? viewport/i
const iSeeTextInViewportPattern = /I (see|expect|expect to see)( (text|content))? "(.*)"(\|(.*))?(.*)? in( (a|the))? viewport/i;
const textInViewportPattern = /((text|content) )?"(.*)"(\|(.*))? is in( (a|the))? viewport/i;
const iSeeElementInViewportPattern = /I (see|expect|expect to see)( (.*))? \((.*)\) in( (a|the))? viewport/i;

module.exports = Object.freeze(
  {
    patterns: {
      viewportPattern,
      elementNotInViewportPattern,
      elementInViewportPattern,
      iSeeTextInViewportPattern,
      iSeeElementInViewportPattern,
      devicePattern,
      textInViewportPattern
    },
    minimumExample: {
      devicePattern: 'Viewport is [device_name]',
      viewportPattern: 'Viewport 1000X1000',
      elementNotInViewportPattern: '(CSS_SELECTOR) is not in viewport',
      elementInViewportPattern: '(CSS_SELECTOR) is in viewport',
      iSeeTextInViewportPattern: 'I see text "value" in viewport',
      iSeeElementInViewportPattern: 'I see (CSS_SELECTOR) in viewport',
      textInViewportPattern: 'Text "value" is in viewport',
    },
    examples: {
      devicePattern: [
        'Viewport is ipad-2',
        'Viewport is ipad-mini',
        'Viewport is iphone-3',
        'Viewport is iphone-4',
        'Viewport is iphone-5',
        'Viewport is iphone-6',
        'Viewport is iphone-6+',
        'Viewport is iphone-7',
        'Viewport is iphone-8',
        'Viewport is iphone-x',
        'Viewport is iphone-xr',
        'Viewport is iphone-se2',
        'Viewport is macbook-11',
        'Viewport is macbook-13',
        'Viewport is macbook-15',
        'Viewport is macbook-16',
        'Viewport is samsung-note9',
        'Viewport is samsung-s10',
        'Viewport is for device samsung-s10',
      ],
      textInViewportPattern: [
        'Text "value" is in viewport',
        'Text "value" is in the viewport',
      ],
      viewportPattern: [
        'Viewport is set to 1000x1000',
        'Viewport dimension is 1000x1000',
        'Viewport 1000x1000',
      ],
      elementNotInViewportPattern: [
        'Element description (CSS_SELECTOR) is not visible in the viewport',
        'Element (CSS_SELECTOR) is not in the viewport',
      ],
      elementInViewportPattern: [
        'Element description (CSS_SELECTOR) is in the viewport',
        'Element (CSS_SELECTOR) is in viewport'
      ],
      iSeeTextInViewportPattern: [
        'I see text "value" in viewport',
        'I see text "value" in the viewport',
        'I expect text "value" in the viewport',
        'I expect content "value" in the viewport',
        'I expect to see text "value" in the viewport',
        'I expect to see content "value" in the viewport',
      ],

      iSeeElementInViewportPattern: [
        'I see (CSS_SELECTOR) in viewport',
        'I expect (CSS_SELECTOR) in viewport',
        'I expect to see (CSS_SELECTOR) in viewport',
        'I see element description (CSS_SELECTOR) in the viewport',
        'I expect element description (CSS_SELECTOR) in the viewport',
        'I expect to see element description (CSS_SELECTOR) in the viewport',
      ]
    },
    usage: {
      viewportPattern: ['Given'],
      elementNotInViewportPattern: ['Then', 'And'],
      elementInViewportPattern: ['Then', 'And'],
      iSeeTextInViewportPattern: ['Then', 'And'],
      iSeeElementInViewportPattern: ['Then', 'And'],
      textInViewportPattern: ['Then', 'And'],
      devicePattern: ['Given', 'And']
    }
  }
);

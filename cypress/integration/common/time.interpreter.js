import { And, Then } from 'cypress-cucumber-preprocessor/steps';
const { waitPattern } = require('./patterns/time.pattern').patterns;

Then(waitPattern, (seconds) => {
  waitForSeconds(seconds);
});

And(waitPattern, (seconds) => {
  waitForSeconds(seconds);
});

const waitForSeconds = (seconds) => {
  cy.wait(seconds * 1000);
}

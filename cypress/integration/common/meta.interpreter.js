import { Then  } from "cypress-cucumber-preprocessor/steps";
const { titlePattern } = require('../common/patterns/meta.pattern').patterns;
const { isMatchCase } = require('../../helpers/textOptions.helper');
const { escapeRegExp } = require('../../helpers/regex.helper');

Then(titlePattern, (title, textOption) => {
  testTitle(title, textOption);
});

And(titlePattern, (title, textOption) => {
  testTitle(title, textOption);
});

const testTitle = (title, textOption) => {
  const keySensitive = isMatchCase(textOption);
  const escapedText = escapeRegExp(title);

  cy.title()
    .then((actualTitle) => {
      if (keySensitive) {
        expect(actualTitle).to.match(new RegExp(escapedText));
      } else {
        expect(actualTitle).to.match(new RegExp(escapedText, 'i'));
      }
    });
}

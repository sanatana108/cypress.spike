import { When, And } from "cypress-cucumber-preprocessor/steps";
const { clickPattern } = require('./patterns/click.pattern').patterns;

When(clickPattern, (optional, selector) => {
  clickOnElement(selector);
});

And(clickPattern, (optional, selector) => {
  clickOnElement(selector);
});

const clickOnElement = (selector) => {
  cy.get(`${selector}`)
    .filter(':visible')
    .first()
    .click();
};

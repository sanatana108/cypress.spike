import { And, Then, Given } from 'cypress-cucumber-preprocessor/steps';
const { isMatchCase } = require('../../helpers/textOptions.helper');

const {
  viewportPattern,
  elementNotInViewportPattern,
  elementInViewportPattern,
  iSeeTextInViewportPattern,
  iSeeElementInViewportPattern,
  devicePattern,
  textInViewportPattern,
} = require('./patterns/viewport.pattern').patterns;

Given(viewportPattern, (optionalText, width, height) => {
  setViewportDimensions(width, height);
});

And(viewportPattern, (width, height) => {
  setViewportDimensions(width, height);
});

Given(devicePattern, (optionalText, optionalText2, deviceName) => {
  setDeviceViewport(deviceName);
});

And(devicePattern, (optionalText, optionalText2, deviceName) => {
  setDeviceViewport(deviceName);
});

Then(iSeeElementInViewportPattern, (action, optionalText, selector) => {
  findElementIsInViewport(selector);
});

And(iSeeElementInViewportPattern, (action, optionalText, selector) => {
  findElementIsInViewport(selector);
});

Then(iSeeTextInViewportPattern, (action, optionalText, content, matchCase) => {
  testISeeTextInViewport(content, matchCase);
});

And(iSeeTextInViewportPattern, (action, optionalText, content, matchCase) => {
  testISeeTextInViewport(content, matchCase);
});

Then(elementNotInViewportPattern, (element, selector) => {
  testElementNotInViewport(selector);
});

And(elementNotInViewportPattern, (element, selector) => {
  testElementNotInViewport(selector);
});

Then(elementInViewportPattern, (element, selector) => {
  testElementInViewport(selector);
});

And(elementInViewportPattern, (element, selector) => {
  testElementInViewport(selector);
});

Then(textInViewportPattern, (optionalText, content, matchCase) => {
  testISeeTextInViewport(content, matchCase);
});

And(textInViewportPattern, (optionalText, content, matchCase) => {
  testISeeTextInViewport(content, matchCase);
});

const testElementInViewport = (selector) => {
  cy.get(`${selector}`)
    .inViewport();
}

const findElementIsInViewport = (selector) => {
  cy.get(`${selector}`)
    .inViewport();
}

const testElementNotInViewport = (selector) => {
  cy.get(`${selector}`)
    .notInViewport()
}

const testISeeTextInViewport = (content, matchCase) => {
  cy.contains(content, { matchCase: isMatchCase(matchCase) })
    .should('be.visible')
    .inViewport();
}

const setDeviceViewport = (deviceName) => {
  cy.viewport(deviceName);
}

const setViewportDimensions = (width, height) => {
  cy.viewport(width, height);
}

const { Then, And } = require('cypress-cucumber-preprocessor/steps');
const { countPattern } = require('./patterns/count.pattern').patterns;
const { getCorrectCondition } = require('../../helpers/countCondition.helper');

Then(countPattern, (condition, optionalText1, count, optionalText3, selector) => {
  testCountElements(condition, count, selector);
});

And(countPattern, (condition, optionalText1, count, optionalText3, selector) => {
  testCountElements(condition, count, selector);
});

const testCountElements = (condition, count, selector) => {
  const assertCondition = getCorrectCondition(condition);
  cy.get(`${selector}`)
    .should(assertCondition, count);
};

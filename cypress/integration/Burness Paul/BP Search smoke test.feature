Feature: BP small smoke test

  As a BP user I want to search for a content.

  Background:
    Given Viewport dimensions 1224x1000

  Scenario: Full user journey
    Given I visit page "https://www.burnesspaull.com/" in browser
    Then I see "Top Tier Scottish Law Firm"|caseinsensitive in the title
    And I see "Burness Paull" in the title

    When I scroll to the bottom of the page
    And I wait 1 second
    Then I see text "Burness Paull LLP is a limited liability partnership of lawyers registered in Scotland" in the viewport

    When I Scroll to the top of the page
    Then I See text "By using this website you agree" in the viewport

    When I scroll to the top of the page
    Then I see search panel & icon (.navigation__wrapper) in the viewport

    When I click on search icon (a[href="https://www.burnesspaull.com/search"])
    Then Url should be "https://www.burnesspaull.com/search"

    When I type "london" into search field (#query)
    Then I see text "London"|keyinsensitive in autocomplete suggestions (.tt-suggestion.tt-selectable)

    When I press enter on search query field (#query)
    Then Url should include "/search?query=london"

    Then I see text "results for"|keyinsensitive
    Then I see text "our people"|keyinsensitive
    Then I see text "related content"|keyinsensitive
    Then Value of search field (#query) is "London"|keyinsensitive
    Then I expect to see at least 8 search results (.news__results .news__item)
    Then I see text "Load more"|keyinsensitive in load more container (.row.loadmore)

    When I scroll to element load more (.row.loadmore)|nocheck
    When I click on load more button (.news__load)
    Then I expect to see at least 15 search results (.news__results .news__item)

    When I scroll to the top of the page
    Then Text "By using this website you agree to our cookie policy for marketing our services and tailoring your online Experience"|keyinsensitive is in the viewport

    When I click on Accept policy button (.cookie-policy__wrapper button)
    Then Text "By using this website you agree to our cookie policy for marketing our services and tailoring your online Experience"|keysensitive is not present

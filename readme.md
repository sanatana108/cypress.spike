# Cypress prototype with cucumber interpreter

## Requirements:

- Node v14
- npm
- Optionally git bash (for running CLI commands)

## First run:

This will install all required npm packages and cypress

- **npm run install:full**

Alternatively you can run:

- **npm install**
and
- **npm run cypress:install**

## To start Cypress:

- **npm run cypress:run** (starts in headless browser)
- **npm run cypress** (opens cypress and allows browser and test selection)
- or **npm run cypress:open** (opens cypress and allows browser and test selection)

### Warning ###
Starting cypress for the first time can take a few moments.
